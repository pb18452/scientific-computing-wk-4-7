
# coding: utf-8

# Solving the ODE: $\frac{dy}{dx}=x$ using the Euler method

# In[8]:


import matplotlib.pyplot as plt
import numpy as np

x = [1]
v = [0]
t = [0]
h = 0.1


# In[9]:


def function(x,t):
    return x


# In[10]:


def euler_step(tstart, xstart, vstart, h):
    print("tstart:", tstart, "xstart:", xstart)
    unew = h*vstart
    print("unew:", unew)
    vnew = h*((0.2*np.sin(tstart)) - (0.1*vstart) - (xstart) - (xstart**3))
    print("vnew:", vnew)
    return unew, vnew

# In[11]:


def rk4(tstart, xstart, vstart, h):
    k1, vk1 = euler_step(tstart, xstart, vstart, h)
    k2, vk2 = euler_step(tstart + h/2, xstart + k1/2,  vstart + vk1/2, h)
    k3, vk3 = euler_step(tstart + h/2, xstart + k2/2, vstart + vk2/2, h)
    k4, vk4 = euler_step(tstart + h, xstart + k3, vstart + vk3, h)
    
    newx = xstart + (1/6)*(k1 + 2*k2 + 2*k3 + k4)
    newv = vstart + (1/6)*(vk1 + 2*vk2 + 2*vk3 + vk4)
    return newx, newv


# In[12]:


def solve_to(x1, v1, t1, t2, hmax, method):
    current_t = t1
    if method == "RK4":
        while current_t <= t2:
            if current_t + hmax <= t2:
                x1, v1 = rk4(current_t, x1, v1, hmax)
                current_t += hmax
            else:
                x1, v1 = rk4(current_t, x1, v1, t2 - current_t)
                current_t += hmax
    """
    if method == "Euler":
        while current_t <= t2:
            if current_t + hmax <= t2:
                x1, v1 += euler_step(current_t, x1, v1, hmax)
                current_t += hmax
            else:
                x1, v1 += euler_step(current_t, x1, v1, t2 - current_t)
                current_t += hmax
    """
    return x1, v1


# In[13]:


def solve_ode(x1, v1, t1, t2, h, method):
    current_t = t1
    current_x = x1
    current_v = v1
    while current_t <= t2:
        current_x, current_v = (solve_to(current_x, current_v, current_t, current_t + h, h/10, method))
        x.append(current_x)
        v.append(current_v)
        current_t += h
        
(solve_ode(1, 0, 0, 150, 0.1, "RK4"))
print(x)
print(v)


# In[14]:


j = h
while len(t) < len(x):
    t.append(j)
    j += h
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.plot(t, x)
plt.show()

