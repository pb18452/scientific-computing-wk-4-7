import matplotlib.pyplot as plt #import packages
import numpy as np
import scipy.optimize as sci
from scipy.integrate import odeint


def f(y, t, e, g, o):
    u, v = y
    dydt = [v, g*np.sin(o*t) - (2*e*v) - (u) - (u**3)]
    return dydt

def shooting(y, ode, t, e, g, o):
    return odeint(ode, y, [0, t], args=(e, g, o))[-1] - y


y0 = [0, 1]

e, g, o = 0.05, 0.2, 1.2
period = 2*np.pi/o
t = np.linspace(0, period, 101)


sol = sci.fsolve(shooting, y0,  args = (f, period, e, g, o))
print(sol)
sol = odeint(f, sol, t, args = (e, g, o))

plt.plot(t, sol[:, 0], 'b', label='u')
plt.plot(t, sol[:, 1], 'g', label='v')
plt.legend(loc='best')
plt.xlabel('t')
plt.grid()
plt.show()
