import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sci


t0, t1 = 0, 2 * np.pi
omega = 1
par = []
n = 500
h = (t1 - t0)/n
x0 = np.zeros(n)

def function(t):
    return np.sin(t)

def xi(i, n):
    return np.cos((i*np.pi)/n)

def ci(i, n):
    if i == 0 or i == n:
        return 2
    else:
        return 1

def cheby(n):
    initm = np.zeros((n + 1, n + 1), dtype = np.float64)
    initm[0][0] = (2*(n**2) + 1)/6
    initm[n][n] = -((2*(n**2) + 1)/6)
    for i in range(n + 1):
        for j in range(n + 1):
            if i == j and initm[i][j] == 0:
                initm[i][j] = (-1 * xi(j, n))/(2*(1 - (xi(j, n)**2)))
            elif i != j and initm[i][j] == 0:
                p = i+j
                initm[i][j] = (ci(i, n)/ci(j, n)) * (((-1)**p)/(xi(i, n) - xi(j, n)))
                
    return initm
    

def Dmatrix(n):
    initm = np.identity(n)
    m1 = -1 * (np.roll(initm, 1, axis=0))
    m2 = np.roll(initm, -1, axis = 0)
    return (1/(2*h)) * (m1 + m2)

def bvector(n):
    mb = np.array([])
    for i in range(n):
        mb = np.insert(mb, i, function(i*h))
    return mb

def time(n):
    time = np.linspace(t0, t1, num=n)
    return time
    
a = Dmatrix(n)
b = bvector(n)

def f(x):
    return a.dot(x) - b + x

x = sci.fsolve(f, x0)
t = time(n)
fig = plt.figure(figsize = (10, 5))
ax = fig.add_subplot(1,2,1)
ax.plot(t,x)
plt.show()

from week5_chebtests import runtests_cheb
runtests_cheb(cheby)
