import matplotlib.pyplot as plt #import packages
import numpy as np
import scipy.optimize as sci


t = [0] #create time list variable to fill for plotting
h = 0.01 #time step
eps = float(input("Please enter epsilon: ")) #enter parameters
gamma = float(input("Please enter gammma: "))
omega = float(input("Please enter omega (0.5 - 1.5): "))
vvalues = [] #empty lists for intital values and values at time = T
xvalues = []
x0 = float(input("What is the initial x?: ")) #input initial x: x0
t1 = float(input("Start time?: "))
t2 = float(input("End time?: "))
v_equation = input("What is the equation for du/dt?: ") #input du/dt or equivalent
dv_dt_equation = input("What is the equation for dv/dt?: ") #input dv/dt or equivalent


#euler step for Duffing equation
def euler_step(t, x, v, h):
    unew = h*eval(v_equation)
    vnew = h*eval(dv_dt_equation)
    #unew = h*v
    #vnew = h*((gamma*np.sin(omega*t)) - (2*eps*v) - (x) - (x**3))
    return unew, vnew


#rk4 method for duffing equation, both x and v
def rk4(tstart, xstart, vstart, h):
    k1, vk1 = euler_step(tstart, xstart, vstart, h)
    k2, vk2 = euler_step(tstart + h/2, xstart + k1/2,  vstart + vk1/2, h)
    k3, vk3 = euler_step(tstart + h/2, xstart + k2/2, vstart + vk2/2, h)
    k4, vk4 = euler_step(tstart + h, xstart + k3, vstart + vk3, h)

    newx = xstart + (1/6)*(k1 + 2*k2 + 2*k3 + k4)
    newv = vstart + (1/6)*(vk1 + 2*vk2 + 2*vk3 + vk4)
    return newx, newv


#numerical solution from t1 to t2
def solve_to(x1, v1, t1, t2, hmax):
    current_t = t1

    while current_t <= t2:
        if current_t + hmax <= t2:
            x1, v1 = rk4(current_t, x1, v1, hmax)
            current_t += hmax
        else:
            x1, v1 = rk4(current_t, x1, v1, t2 - current_t)
            current_t += hmax

    return x1, v1


#solving the ode with initial position, velocity, time to t2 with step size h
def solve_ode(x1, v1, t1, t2, h):
    current_t = t1
    current_x = x1
    current_v = v1
    x = [x1]
    while current_t <= t2:
        current_x, current_v = (solve_to(current_x, current_v, current_t, current_t + h, h/10))
        x.append(current_x)
        current_t += h
    return x


#create a list of times for plotting
def time_list(t1, t2):
    n = np.ceil((t2 - t1)/h)
    list = np.linspace(t1, t2, num=n+2)
    return list


def f(v):
    return solve_ode(x0, v, t1, t2, h)[-1] - x0

v = sci.fsolve(f, 0)
print(v)

#plot results
fig = plt.figure(figsize = (10, 5))
ax = fig.add_subplot(1,1,1)
ax.set_xlabel("X")
ax.set_ylabel("t")
ax.plot(time_list(t1, t2), solve_ode(x0, v, t1, t2, h))
plt.show()
