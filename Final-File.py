import matplotlib.pyplot as plt #import packages
import numpy as np
import scipy.optimize as sci
from scipy.integrate import odeint
init = False #initialisation boolean for custom_ode

def mse(y, t, k, pars): #some default ode's to try, this is the mass spring ode
    u, v = y #takes a vector (u, v) and splits it up into 2 variables
    dydt = [v, np.sin(np.pi*t) - 2*pars[0]*v - k*u] #assigns new values to those variables
    return dydt #returns the new variables

def duffing(y, t, k, pars): #duffing ode
    u, v = y #see above
    dydt = [v, pars[2]*np.sin(pars[1]*t) - (2*pars[0]*v) - (k*u) - (u**3)]
    return dydt

def custom_ode(y, t, k, pars):
    global init #imports the global boolean

    if init == False: #checks to see whether the information needed for this function has already been defined or not
        print("Welcome to custom ode solver! /n Please input odes in the form of du/dt and dv/dt")
        print("For example du/dt = v  and dv/dt = pars[2]*np.sin(pars[1]*t) - (2*pars[0]*v) - (k*u) - (u**3)")
        print("You can input up to 4 parameters with k being varied over time! (Enter 1 if not using all the parameters)") #information about inputs
        global dudt #initialising global variables du/dt and dv/dt
        dudt = input("What is du/dt?: ")
        global dvdt
        dvdt = input("What is dv/dt?: ")
        init = True #set init to true so we dont ask for these every loop

    u, v = y
    dydt = [eval(dudt), eval(dvdt)]
    return dydt #see above, same as before

def shooting(y, ode, t, k, pars): #using shooting with ode int to find the u, v at time = T
    return np.array(odeint(ode, y, t, args=(k ,pars))[-1] - y) #returns uv(T) - uv(0) where uv is the vector (u, v)

def angle(v1, v2): #was planning to have a check to see the angle between the guessing vecot and the actual vector to see how much the algoirthm were "turning" and change secant
    return np.arccos(v1.dot(v2)/(np.linalg.norm(v1)*np.linalg.norm(v2))) #simple equation for calculating the angle

def aug(y, ode, period, pars, yhat, secant): #returns the u,v and new paramter that shooting finds
    temp = np.append(shooting(y[0:2], ode, period, y[2], pars), secant@(y - yhat)) #makes sure that both the shooting equation is satisfied and also that we search at a right angle to the secant
    return temp

ode = input("Which ode would you like to use?(mse/duffing/custom_ode): ") #asking to enter which ode you want to use
pars = [0,0,0] #initialise the paramters, would like to ask how many and let the user enter all of the parameters they require
pars[0] = float(eval(input("What is the first parameter? ")))
pars[1] = float(eval(input("What is the second parameter?(omega) ")))
pars[2] = float(eval(input("What is the third parameter? ")))
kstart = float(input("Varied parameter start?(Can't be 0 for mse equation!): ")) # start point for your changing parameter
kend = float(input("Varied parameter end?: ")) #end for changing parameter
y0 = [0, 0] #start point for u and v
period = 2*np.pi/pars[1] #period that we are searching over, relies on parameter 2
t = np.linspace(0, period, 101) #times over the period


p0 = kstart
uv0 = sci.fsolve(shooting, y0, args = (eval(ode), t, p0, pars)) #calculates the first u,v for first value of p where p is the changing parameter

p1 = p0 + 0.1
uv1 = sci.fsolve(shooting, uv0, args = (eval(ode), t, p0, pars)) #calculating the second u,v from the first while guessing the parameter step (natural continuation)

#step = 1 was going to alter step size based on angle
uu = np.append(uv0[0], uv1[0]) #add u's to a list called uu so far
vv = np.append(uv0[1], uv1[1]) #add v's to list called vv so far
pp = [p0, p1] #add parameter points so far
y0 = np.append(uv0, p0) #first (u, v, p) vector
y1 = np.append(uv1, p1) # second (u, v, p) vector

while pp[-1] <= kend: #while our parameter is less than our final paramter
    secant = y1 - y0 #calculate secant for all 3 dimensions (u, v, p)
    y2hat = y1 + secant #guess next y vector
    y2 = sci.fsolve(aug, y2hat, (eval(ode), t, pars, y2hat, secant)) #correct
    uu = np.append(uu, y2[0]) #add new u, v, p to lists
    vv = np.append(vv, y2[1])
    pp = np.append(pp, y2[2])
    #a = angle(y1 - y0, y2 - y1) adjust step size based on angle (broken)
    #step = ((np.pi - (2 * a))/np.pi)
    y0 = y1 #rotate variables
    y1 = y2

newu = [] #define set of new u values
for i in range(len(uu)):
    newu.append(np.max(odeint(eval(ode), [uu[i],vv[i]], t, args = (pp[i], pars)))) #finds the max(u) for each time series with starts of u, v we had before


plt.plot(pp, np.abs(newu), "b", label = ode) #plots the parameter against the max(u)
plt.legend(loc='best')
plt.xlabel('k')
plt.ylabel("||u||")
plt.show()
