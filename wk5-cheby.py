import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as sci

n = 100 #number of nodes
u = np.zeros(n + 1) #initialising cheby points
kstart = 1
kend = 1
pars = [0,0,0]
pars[0] = float(eval(input("What is the first parameter? ")))
pars[1] = float(eval(input("What is the second parameter? ")))
pars[2] = float(eval(input("What is the third parameter? ")))
print("Remeber to put k as the paramter which you are varying!")
fxpart = input("What is the f(x) part of the ode?(i.e pars[2]*np.sin(pars[1]*t) - k*x - x**3): ")
fxdotpart = input("What is the f(x dot) part of the ode?(not inlcuding x dot istself i.e the parameter) ")
fxddotpart = input("What is the f(x double dot) part of the ode?(not including x double dot itself, i.e the parameter) ")

#simple part of the ode
def fx(x, t, k, pars):
    return eval(fxpart)

#dx/dt
def fxdot(x, t, k, pars):
    return eval(fxdotpart) * chebymm(x)

#d^2x/dt^2
def fxddot(x, t, k, pars):
    return eval(fxddotpart) * chebymm(chebymm(x))

#calcualting individual cheby points
def xi(i, n):
    return np.cos((i*np.pi)/n)

#calculating constants for cheby matrix
def ci(i, n):
    if i == 0 or i == n:
        return 2
    else:
        return 1

#forming the cheby matrix
def cheby(n):
    initm = np.zeros((n + 1, n + 1), dtype = np.float64)
    initm[0][0] = (2*(n**2) + 1)/6
    initm[n][n] = -((2*(n**2) + 1)/6)
    for i in range(n + 1):
        for j in range(n + 1):
            if i == j and initm[i][j] == 0:
                initm[i][j] = (-1 * xi(j, n))/(2*(1 - (xi(j, n)**2)))
            elif i != j and initm[i][j] == 0:
                p = i+j
                initm[i][j] = (ci(i, n)/ci(j, n)) * (((-1)**p)/(xi(i, n) - xi(j, n)))

    return initm

#creating a list of cheby points
def chebypoints(n):
    time = np.array([])
    for i in range(n + 1):
        time = np.insert(time, i, xi(i, n))
    return time

#matrix multiplication with initial condition
def chebymm(u):
    Du = a@u
    Du[-1] = Du[0]
    return Du

#function to solve
def f(y, u, k, pars):
    #print(fxddot(y, u, k, pars) - fxdot(y, u, k, pars) - fx(y, u, k, pars))
    return fxddot(y, u, k, pars) - fxdot(y, u, k, pars) - fx(y, u, k, pars)


#input
def collocation(n, u, k, pars):
    y = sci.fsolve(f, u, (u, k, pars))
    return y

#initialise start points and cheby matrix
def init(n):
    u = chebypoints(n)
    a = cheby(n)
    return u, a
u, a = init(n)

#calculate points
k = kstart
while k <= kend:
    y = collocation(n, u, k, pars)
    k =+ 0.1

#plot
fig = plt.figure(figsize = (10, 5))
ax = fig.add_subplot(1,2,1)
ax.plot(u, y, color="b")
plt.show()
